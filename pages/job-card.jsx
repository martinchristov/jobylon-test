import React, { useState } from 'react'
import { Button, Card, Col, Divider, Tag } from 'antd'
import moment from 'moment'
import Image from 'next/image'
import { DownOutlined, LinkedinFilled, LinkedinOutlined, MailFilled } from '@ant-design/icons'
import classNames from 'classnames'

const MapPin = () => (
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlnsXlink="http://www.w3.org/1999/xlink" xmlnsSvgjs="http://svgjs.com/svgjs" viewBox="0 0 24 24" height="24" width="24">
    <g transform="matrix(1,0,0,1,0,0)">
      <path d="M 21,10.063c0,6.227-7.121,12.058-8.695,13.27c-0.18,0.138-0.43,0.138-0.61,0C10.122,22.122,3,16.29,3,10.063 c0-5.246,3.753-9.5,9-9.5S21,4.817,21,10.063z " strokeLinecap="round" strokeLinejoin="round" />
      <path d="M 12,4.563c2.761,0,5,2.239,5,5s-2.239,5-5,5s-5-2.239-5-5S9.239,4.563,12,4.563z" strokeLinecap="round" strokeLinejoin="round" />
    </g>
  </svg>
)

export const JobHead = ({item}) => (
  <div>
    {item.title} <Tag>{item.experience}</Tag><br />
    <ul>
      <li><div className="loc-pin"><Image alt="loc" width={12} height={12} src="/location.svg" /></div> {item.locations[0].location.text}</li>
      <li>{item.employmentType}</li>
      <li>from {moment(item.fromDate, 'YYYY-MM-DD').format('DD MMM')}</li>
    </ul>
  </div>
)

const JobCard = ({ item, setModalItem, inModal }) => {
  const [open, setOpen] = useState(false)
  const handleReadMore = () => {
    console.log(window.innerWidth)
    if(window && window.innerWidth < 1024){
      setOpen(!open)
    } else {
      setModalItem(item)
    }
  }
  if(!item) return null

  const content = (
    <div className={classNames('h-constrainer job-body', { open })}>

      <div dangerouslySetInnerHTML={{ __html: item.descr }} />
      <Divider />
      <h4>Skills & Requirements</h4>
      <div dangerouslySetInnerHTML={{ __html: item.skills }} />
      <Divider />
      <h4>About the company</h4>
      <Card.Meta
        title={
          <div>
            <a href={item.company.website} target="blank">{item.company.name}</a>
            &nbsp;<Tag color={item.company.industry === 'Startup' ? 'lime' : null}>{item.company.industry}</Tag>
            <a href={`https://linkedin.com/company/${item.linkedInCompanyId}/`} target="_blank" rel="noreferrer"><LinkedinFilled /></a>
          </div>
        }
        avatar={<img src={item.company.logo} width={50} />}
        description={<p dangerouslySetInnerHTML={{ __html: item.company.descr }} />}
      />
      <Divider />
      <div className="contact">
        <h5>{item.contact.name} | Contact Person</h5><br />
        <a href={`mailto:${item.contact.email}`}><Button type="link"><MailFilled /> Send Email</Button></a>
      </div>
      <div className="primary-btn-holder">
        <a href={item.urls.apply}><Button type="primary" size="large">{`I'm Interested`}</Button></a>
      </div>
    </div>
  )
  if(inModal) return content
  return (
    <Col key={item.id} span={8} xs={24} md={12} lg={8} xxl={6}>
      <Card
        title={<JobHead item={item} />}
        cover={<img src={item.company.cover} alt={item.company.name} />}
      >
        {content}
        <div className="readmore-btn" onClick={handleReadMore}>
          Read {open ? 'less' : 'more'} <DownOutlined style={open ? { transform: 'rotate(180deg)'} : null} />
        </div>
      </Card>
    </Col>
  )
}

export default JobCard