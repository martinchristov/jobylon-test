import { Row, Layout, Modal } from 'antd'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import axios from 'axios'
import humps from 'humps'
import JobCard, {JobHead} from './job-card'

const { Content } = Layout;

const config = {
  params: { format: 'json' },
  transformResponse: [
    ...axios.defaults.transformResponse,
    data => humps.camelizeKeys(data)
  ],
  transformRequest: [
    data => humps.decamelizeKeys(data),
    ...axios.defaults.transformRequest
  ]
}

const Home = () => {
  const [items, setItems] = useState([])
  const [modalItem, setModalItem] = useState(null)
  useEffect(() => {
    axios({
      method: 'get',
      url: 'https://feed.jobylon.com/feeds/7d7e6fd12c614aa5af3624b06f7a74b8/?format=json',
      ...config
    })
    .then((d) => {
      console.log(d)
      setItems(d.data)
    })
  }, [])
  return (
    <Layout>
      <Head>
        <title>Jobylon test feed</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Content>
        <Row className="joblist">
          {items.map(item => <JobCard key={item.id} item={item} setModalItem={setModalItem} />)}
        </Row>
      </Content>
      <Modal className="job-modal" width={780} title={modalItem ? <JobHead item={modalItem} /> : ''} visible={modalItem != null} footer={null} onCancel={() => setModalItem(null)}>
        {modalItem && <JobCard item={modalItem} inModal />}
      </Modal>
    </Layout>
  )
}

export default Home